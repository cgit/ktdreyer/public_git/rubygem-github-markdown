%global gem_name github-markdown

Name: rubygem-%{gem_name}
Version: 0.6.4
Release: 1%{?dist}
Summary: The Markdown parser for GitHub.com
Group: Development/Languages
License: ISC
URL: https://github.github.com/github-flavored-markdown/
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: ruby
Requires: ruby(rubygems) 
BuildRequires: ruby(release)
BuildRequires: rubygems-devel 
BuildRequires: ruby-devel 
BuildRequires: rubygem(minitest)
BuildRequires: rubygem(nokogiri)
Provides: rubygem(%{gem_name}) = %{version}

%description
Self-contained Markdown parser for GitHub, with all GitHub's custom extensions


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

# Clean up development-only file
rm Rakefile
sed -i "s|\"Rakefile\",||g" %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

%gem_install

# Remove unnecessary gemspec file
rm .%{gem_instdir}/%{gem_name}.gemspec

%install
mkdir -p %{buildroot}%{gem_dir}
cp -pa .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

mkdir -p %{buildroot}%{_bindir}
cp -pa .%{gem_instdir}/bin/gfm \
        %{buildroot}%{_bindir}/

# Remove deprecated "ext" directory
rm -r %{buildroot}%{gem_instdir}/ext

# Move the binary extension
mkdir -p %{buildroot}%{gem_extdir_mri}/lib/github
mv %{buildroot}%{gem_libdir}/github/markdown.so %{buildroot}%{gem_extdir_mri}/lib/github


%check
pushd .%{gem_instdir}
  testrb -Ilib test/gfm_test.rb
popd

%files
%dir %{gem_instdir}
%{_bindir}/gfm
%{gem_instdir}/bin
%{gem_libdir}
%{gem_extdir_mri}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%exclude %{gem_instdir}/test

%changelog
* Sun Feb 16 2014 Ken Dreyer <ktdreyer@ktdreyer.com> - 0.6.4-1
- Update to github-markdown 0.6.4
- Require: ruby (not ruby(release)) since this is a C extension

* Sat Dec 28 2013 Ken Dreyer <ktdreyer@ktdreyer.com> - 0.6.3-1
- Initial package
